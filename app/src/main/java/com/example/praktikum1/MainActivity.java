package com.example.praktikum1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText lebar, panjang;
    TextView answer;
    Button cek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lebar=(EditText) findViewById(R.id.lebar);
        panjang=(EditText) findViewById(R.id.panjang);
        cek=(Button)findViewById(R.id.cek);
        answer=(TextView)findViewById(R.id.hasil);

        cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(lebar.getText().toString());
                int b = Integer.parseInt(panjang.getText().toString());
                int c = a*b;
                answer.setText(String.valueOf(c));
            }
        });
    }
}